import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Datavalidator {

    WebDriver driver;

    @BeforeTest
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "C:\\usr\\local\\bin\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Test(priority = 0)
    public void test() throws InterruptedException {
        List<String> keyss = Arrays.asList("data That needs to be validated","If data is more, external source like CSV, or JSON can be used");
        for (String s : keyss) {
            boolean a = equalLists(getDBData(s), getUIData(s));
            if (a) {
                System.out.println("DB Data " + getDBData(s));
                System.out.println(" UI Data " + getUIData(s));
                System.out.println("List is equal for >>>>>>>>>>>.........................................." + s);
            } else {
                System.out.println(" DB Data " + getDBData(s));
                System.out.println(" UI Data " + getUIData(s));
                System.out.println("List is not equal for <<<<<<<<<<........................................" + s);
                System.out.println("****************************************************************");

            }
        }
    }

    public boolean equalLists(List<String> one, List<String> two) {
        if (one == null && two == null) {
            return true;
        }
        if (one == null || two == null || one.size() != two.size()) {
            return false;
        }
        //to avoid messing the order of the lists we will use a copy
        one = new ArrayList<String>(one);
        two = new ArrayList<String>(two);
        System.out.println("DB Contains UI " + one.containsAll(two));
        System.out.println("UI Contains DB " + two.containsAll(one));
        return one.containsAll(two) && two.containsAll(one);
    }

    public List<String> getDBData(String keyname) {
        String QUERY = "select * from table where column='" + keyname + "'";
        List<String> nameList = new ArrayList<String>();
        String url = "jdbc:mysql://IPAddress/" + "database_name" + "?user=uname&password=password&autoReconnect=true&useSSL=false&useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=GMT";
        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(QUERY);) {
            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();
            while (rs.next()) {
                for (int columnIndex = 2; columnIndex <= columnCount - 2; columnIndex++) {
                    String a = ((rs.getString(columnIndex) == null) ? "" : rs.getString(columnIndex));
                    String b = a.replace("\n", "").replace("\r", "");
                    nameList.add(b);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameList;
    }


    public List<String> getUIData(String keyname) throws InterruptedException {
        driver.get("https://UIURLWhereDataNeedstobescrappedfrom");
        WebElement uname = driver.findElement(By.id("username"));
        uname.sendKeys("abhishektiwari"); // send also a "\n"
        WebElement pwd = driver.findElement(By.id("password"));
        pwd.sendKeys("password"); // send also a "\n"
        WebElement submit = driver.findElement(By.xpath("//*[@id=\"appRoot\"]/section/main/div/form/div[3]/div/div/span/button/span"));
        submit.submit();
        WebElement search = driver.findElement(By.name("query"));
        search.sendKeys(keyname); // send also a "\n"
        search.submit();

        List<String> nameList = new ArrayList<String>();
        List<WebElement> findElements = driver.findElements(By.xpath("//*[@id=\"appContainer\"]/div/main/table/tbody/tr/td[3]/div"));
        for (WebElement webElement : findElements) {
            String values = webElement.getAttribute("textContent");
            String b = values.replace("\n", "").replace("\r", "");
            nameList.add(b);
        }
        WebElement page2 = driver.findElement(By.xpath("//*[@id=\"appContainer\"]/div/main/nav/ul/li[4]"));
        page2.click();
        Thread.sleep(3000);
        List<WebElement> findElements1 = driver.findElements(By.xpath("//*[@id=\"appContainer\"]/div/main/table/tbody/tr/td[3]/div"));
        // this are all the links you like to visit
        for (WebElement webElement : findElements1) {
            String values = webElement.getAttribute("textContent");
            String b = values.replace("\n", "").replace("\r", "");
            nameList.add(b);
        }
        search.clear();
        WebElement logout = driver.findElement(By.xpath("//*[@id=\"appContainer\"]/div/header/div[2]/button"));
        logout.click();
        return nameList;
    }


    @AfterTest
    public void teardown() {
        driver.close();
    }
}